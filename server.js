
const express        = require('express');
const MongoClient    = require('mongodb').MongoClient;
const dbSqlite3      = require('sqlite3').verbose();
const bodyParser     = require('body-parser');

const app            = express();
const db             = require('./config/db');

const port = 8000;

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));

const dbSqlite3 = new sqlite3.Database(':memory:');

MongoClient.connect(db.url, (err, database) => {
  
  if (err) return console.log(err)

  // If you’re using the latest version of the MongoDB (3.0+)
  // make sure you add the database name and not the collection name
  // db = database.db("note-api")

  require('./app/routes')(app, database);
  app.listen(port, () => {
  	console.log('We are live on ' + port);
  });   

})
